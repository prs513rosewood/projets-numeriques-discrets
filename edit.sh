#!/usr/bin/env bash

source $VENVS/discret/bin/activate
/opt/quarto/bin/quarto preview book &
jupyter lab
