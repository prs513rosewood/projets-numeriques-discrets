{
 "cells": [
  {
   "cell_type": "raw",
   "id": "95a03ebe-e287-433f-bcf0-eaa21993cc4b",
   "metadata": {
    "editable": true,
    "raw_mimetype": "",
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "---\n",
    "format:\n",
    "    html:\n",
    "        code-fold: false\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a6b0207-681f-4744-8dd0-a59c875049ab",
   "metadata": {},
   "outputs": [],
   "source": [
    "#| echo: false\n",
    "\n",
    "%config InlineBackend.figure_formats = ['svg']\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "730fbd8f-89af-4213-ab23-ff7fc488529c",
   "metadata": {},
   "source": [
    "# 2/ Collisions et interactions\n",
    "\n",
    "![Collision de billard](figs/pool)\n",
    "\n",
    "Dans ce TP, nous allons enfin introduire une physique pour les interactions entre particules. Nous allons commencer par de simples collisions élastiques qui vont nous permettre de calculer des forces entre les particules, et discuter du potentiel d'interaction harmonique.\n",
    "\n",
    "#### Objectifs\n",
    "\n",
    "- Calculer la force à partir d'un potentiel\n",
    "- Calculer les distances pour toutes les paires de particules\n",
    "- Calculer les forces associées à chaque paire\n",
    "- Inclure le calcul des forces dans l'intégration des équations de mouvement\n",
    "- Calculer une collision avec une paroi\n",
    "\n",
    "## Prélude : organisation du code\n",
    "\n",
    "Dans le [précédent TP](balistique.ipynb), nous avons implémenté les deux fonctions ci-dessous :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f562d3b-c259-4ec2-b088-fdbde26e979e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import numpy.typing as npt\n",
    "\n",
    "def verlet_predictor(positions: npt.NDArray[float],\n",
    "                     velocities: npt.NDArray[float],\n",
    "                     masses: npt.NDArray[float],\n",
    "                     forces: npt.NDArray[float],\n",
    "                     dt: float):\n",
    "    \"\"\"Étape de prédiction de Velocity-Verlet\"\"\"\n",
    "\n",
    "def verlet_corrector(positions: npt.NDArray[float],\n",
    "                     velocities: npt.NDArray[float],\n",
    "                     masses: npt.NDArray[float],\n",
    "                     forces: npt.NDArray[float],\n",
    "                     dt: float):\n",
    "    \"\"\"Étape de correction de Velocity-Verlet\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2bb05e6d-7f62-4e15-b507-fbff81178649",
   "metadata": {},
   "source": [
    "Vous pouvez déplacer ces fonctions dans leur propre fichier `my_functions.py`. Dans le script que vous écrirez pour le présent TP (et les suivants), vous pourrez écrire le code suivant au début de votre script pour pouvoir utiliser ces fonctions. Voir @sec-conseils-python.\n",
    "\n",
    "```{python}\n",
    "import sys\n",
    "sys.path.append('.')\n",
    "\n",
    "from my_functions import *\n",
    "```\n",
    "\n",
    ":::{.callout-warning}\n",
    "Il faut que le fichier `my_functions.py` se trouve dans le même dossier que votre fichier de TP.\n",
    ":::\n",
    "\n",
    "## Contact entre sphères élastiques\n",
    "\n",
    "![Distances entre deux particules de rayon $R$](figs/ball_distances.svg){#fig-ball-distances}\n",
    "\n",
    "Le problème du contact entre deux sphères est, dans le cas général^[qui prend en compte la rugosité, le frottement, l'adhésion, etc.], trop complexe pour être résolu à chaque collision de sphère. On va donc poser un cadre qui conservera une formulation simple, prenons deux sphères de rayons $R$, de propriétés élastiques $(E, \\nu)$, supposons un contact sans frottement et sans adhésion, et le rayon de contact (l'aire de contact est un disque) petit par rapport aux rayons des sphères. Ce problème a été résolu en 1881 par Hertz, qui a pu calculer la distance de chevauchement (voir @fig-ball-distances) $\\delta = 2R - d_{12}$ où $d_{12}$ est la distance entre les centres des deux sphères :\n",
    "\n",
    "$$ \\delta = \\left(\\frac{9P^2}{16R{E^*}^2}\\right)^\\frac{1}{3},\\quad \\mathrm{avec}\\ E^* = \\frac{E}{2(1 - \\nu^2)} $$\n",
    "\n",
    "$P$ est la force totale qui joint les deux sphères. En inversant cette relation, on peut exprimer la force en fonction de la distance $\\delta$ :\n",
    "\n",
    "$$ P = \\frac{4}{3}E^* \\delta\\sqrt{R\\delta}$$\n",
    "\n",
    "Notons que dans la théorie de Hertz, la quantité $\\sqrt{R\\delta}$ est le rayon de l'aire de contact. Une autre façon très commune d'exprimer la force de contact est via un ressort de rigidité $k_\\alpha = \\alpha E^*R$, $\\alpha$ étant sans dimension et généralement petit :\n",
    "\n",
    "$$ P = k_\\alpha\\delta $$\n",
    "\n",
    "Comparons les courbes de ces équations adimensionnalisées :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abeaaf6e-8c2d-473a-87ae-331a6e51489b",
   "metadata": {},
   "outputs": [],
   "source": [
    "#| echo: false\n",
    "#| fig-cap: Force de rappel pour collision élastique\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "\n",
    "alpha = 0.25\n",
    "delta = np.linspace(0, alpha, 200)\n",
    "\n",
    "ax.plot(delta, 4/3 * delta * np.sqrt(delta), 'r', label='Hertz')\n",
    "ax.plot(delta, alpha * delta, 'k', label=f'Ressort, $\\\\alpha = {alpha:.2f}$')\n",
    "ax.legend()\n",
    "ax.set(\n",
    "    xlabel='$\\\\delta / R$', ylabel='$P / (E^*R^2)$',\n",
    "    xlim=(0, alpha), ylim=(0, alpha**2),\n",
    ")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5477b14a-5f93-4b04-bcfb-196e4ba05bee",
   "metadata": {},
   "source": [
    "Il est intéressant de noter que la courbe de Hertz a une dérivée nulle pour $\\delta = 0$, et que la rigidité du contact $\\frac{\\partial P}{\\partial \\delta}$ augmente alors \n",
    "que la modélisation du contact par un ressort suppose une rigidité constante. \n",
    "\n",
    "## Quantités conservées\n",
    "Dans toute simulation, il faut s'assurer que la quantité de mouvement est bien conservée au cours du temps. Dans notre cas, puisque l'on cherche à modéliser des chocs élastiques, il faut *aussi* que l'énergie cinétique soit conservée durant la simulation.\n",
    "\n",
    "\n",
    "## Matrice de distance, calcul naïf de l'interaction\n",
    "\n",
    "![Matrice des distances (gauche), vecteurs forces entre particules (centre) et forces totales par particule (droite).](figs/forces.svg){#fig-distance-forces}\n",
    "\n",
    "Puisqu'on doit calculer les forces qui agissent entre toutes les particules (et qui dépendent de la distance, voir @fig-distance-forces), on doit calculer les distances entre toutes les particules. Définissons la position de 4 particules :\n",
    "\n",
    ":::{.callout-warning}\n",
    "Le code ci-dessous est donné à titre d'exemple, il ne fait pas partie des consignes du TP.\n",
    ":::"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eab26444-3af7-4beb-80bd-7a389f74f5dd",
   "metadata": {},
   "outputs": [],
   "source": [
    "r = np.array([\n",
    "    [0, 1, 0, 0.5], # positions en x\n",
    "    [0, 0, 1, 1],   # positions en y\n",
    "])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20fc6f78-b371-4500-aaac-00e4534445a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "#| echo: false\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(*r, marker='o', markersize=20, ls='', color='k')\n",
    "\n",
    "for i, x in enumerate(r.T):\n",
    "    ax.annotate(str(i), x, color='w', ha='center', va='center')\n",
    "\n",
    "ax.set(xlabel='$x$', ylabel='$y$')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c6c9455-4f24-47c5-b071-9f5eb7d55a4d",
   "metadata": {},
   "source": [
    "On cherche donc à calculer le vecteur $\\vec{r}_{ij} = \\vec{r}_j - \\vec{r}_i$ pour chaque $i$ et chaque $j$. Une façon de procéder serait d'écrire deux boucles imbriquées."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32eea688-9124-44c3-84b1-304249260ea9",
   "metadata": {},
   "outputs": [],
   "source": [
    "N = r.shape[1]  # le nombre de particules\n",
    "D = r.shape[0]  # la dimension physique du problème (ici 2D)\n",
    "rij = np.zeros([D, N, N]) # on définit la matrice des vecteurs entre particules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abc85f81-ed97-4856-abb8-2469963d3476",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit # <- ne pas copier dans un script\n",
    "for i in range(N):\n",
    "    for j in range(N):\n",
    "        rij[:, i, j] = r[:, j] - r[:, i]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0dcf8ed8-8aa0-425f-a1bc-ff94e6267064",
   "metadata": {},
   "outputs": [],
   "source": [
    "rij"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec23ccf6-1efb-4b9e-9c84-9bc7034985c1",
   "metadata": {},
   "source": [
    "On peut vérifier que les distances sont bien celles attendues:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "928f6758-518c-4360-acd5-b1a934c9a8e6",
   "metadata": {},
   "outputs": [],
   "source": [
    "dij = np.linalg.norm(rij, axis=0)  # calcul des normes pour chaque colonne de rij (axis=0 <=> norme sur les colonnes)\n",
    "dij"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c16a92ce-4c2f-492e-8bdd-eb4ca55ed8aa",
   "metadata": {},
   "source": [
    "Cependant, les boucles `for i in range(N)` sont très lentes en python. Comme on utilise Numpy, on peut se servir de la fonctionnalité de \"broadcast\" pour éviter d'écrire des boucles:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f7d861b-ead2-43aa-b5aa-5204e02317c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit # <- ne pas copier dans un script\n",
    "rij_ = r[:, np.newaxis, :] - r[:, :, np.newaxis]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a6c01d5-39bd-449f-8b7a-4004a11b88fd",
   "metadata": {},
   "source": [
    "On a gagné un facteur 10 sur la vitesse d'exécution ! Vérifions tout de même que le résultat est correct."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e27ffa08-005e-47da-8ef4-4615d6364fda",
   "metadata": {},
   "outputs": [],
   "source": [
    "rij_ = r[:, np.newaxis, :] - r[:, :, np.newaxis]\n",
    "print(np.all(rij == rij_))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1ded1f0-5a81-401f-9aae-3ce3161c002b",
   "metadata": {},
   "source": [
    "::: {.callout-caution title=\"Consigne\"}\n",
    "En utilisant le code ci-dessus, implémentez la fonction ci-dessous :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d09b19b9-2c8f-4213-bed5-2085c8194e9d",
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_dij_nij(positions: npt.NDArray[float]):\n",
    "    \"\"\"Calcule les distances et vecteurs normaux pour des paires de particules\"\"\"\n",
    "    dij = ... # distances entre paires de particules ||r_ij||\n",
    "    nij = ... # vecteurs normaux r_ij / ||r_ij||\n",
    "    return dij, nij"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "249364ec-20a2-435c-b3dd-db4b121bafc2",
   "metadata": {},
   "source": [
    "**Attention** : la matrice `dij` calculée ci-dessus contient des zéros sur la diagonale. Le calcul de $\\vec n_{ij} = \\vec r_{ij} / \\|\\vec r_{ij}\\|$ contient donc des divisions par zéro qui produisent des [*NaN*](https://fr.wikipedia.org/wiki/NaN) (*Not a Number*, pas un nombre). Ces NaNs sont des nombres flottants spéciaux qui se propagent quand on fait des calculs qui les contiennent. Ils sont très utiles pour déboguer un code car ils signalent une erreur probable. Dans notre cas, en revanche, on doit s'en débarrasser avant de procéder à d'autres calculs. On peut le faire avec la fonction `np.nan_to_num` comme ceci :\n",
    "\n",
    "```python\n",
    "np.nan_to_num(nij, copy=False, nan=0.)\n",
    "```\n",
    "Vérifiez que ce que retourne la fonction `compute_dij_nij` ne contient pas de NaN.\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3a35662-f7f0-417f-94d3-4a2e809db035",
   "metadata": {},
   "source": [
    "## Potentiels d'interaction\n",
    "\n",
    "On va dans un premier temps choisir la relation linéaire entre $P$ et $\\delta$. En supposant que toutes les sphères aient le même rayon $R$, on peut écrire l'énergie potentielle d'une paire de sphères sous la forme :\n",
    "\n",
    "$$ U_2(d_{ij}) = \\frac{1}{2}k_\\alpha (2R - d_{ij})^2H(2R - d_{ij}) = \\begin{cases} \\frac{1}{2}k_\\alpha (2R - d_{ij})^2 & \\text{si } 2R \\geq d_{ij}\\\\ 0 & \\text{sinon}\\end{cases} $$\n",
    "où $H:\\mathbb{R}\\rightarrow \\mathbb{R}$ est la fonction d'Heaviside :\n",
    "\n",
    "$$ H(x) = \\begin{cases} 1 & \\mathrm{si}\\ x \\geq 0\\\\\n",
    "0 & \\mathrm{sinon}\n",
    "\\end{cases} $$\n",
    "\n",
    "On utilise la fonction d'Heaviside dans l'énergie potentielle pour exprimer le fait que si deux sphères ne se touchent pas (si $d_{ij} > 2R$) alors l'énergie potentielle d'interaction est nulle. On implémente maintenant le calcul de la force dérivée^[Le calcul de la dérivée est à faire à la main en ignorant la fonction $H(x)$.] au-dessus.\n",
    "\n",
    ":::{.callout-caution title=\"Consigne\"}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "864d2099-ae6a-4435-ae94-60edbf9b42b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "def elastic_bounce_force(dij: npt.NDArray[float], R: float, ka: float):\n",
    "    \"\"\"Calcule l'intensité de la force de rappel élastique entre les particules, -U_2'(dij)\"\"\"\n",
    "    return ..."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "78e0c2f4-bb4c-4adb-a6b1-139451cc78d4",
   "metadata": {},
   "source": [
    "Implémentez la fonction ci-dessus en utilisant la fonction $U_2'$ dérivée plus haut. Pour intégrer la fonction d'Heaviside dans le calcul, on peut se servir du tableau de booléens suivant :\n",
    "\n",
    "```python\n",
    "H = (2 * R - dij) > 0 # H contient p.ex. [True False ... True]\n",
    "```\n",
    "Multiplier par `H` revient à multiplier par la fonction d'Heaviside $H(2R - d_{ij})$.\n",
    "\n",
    "Enfin, on peut combiner le calcul de `rij` et le calcul des forces en une fonction (sans oublier que $\\vec f_i = \\sum_{k=1,\\ k\\neq i}^N \\vec f_{ki}$, que l'on peut calculer avec `np.sum`) :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a310ddd-870d-4cd7-8d5f-193ad77fe5fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_forces_collision(positions: npt.NDArray[float],\n",
    "                             R: float, ka: float):\n",
    "    \"\"\"Calcule les forces sur chaque particule\"\"\"\n",
    "    # Étape 1: calcul de dij et nij\n",
    "    # Étape 2: calcul de -U_2'(dij) avec la fonction elastic_bounce_force\n",
    "    # Étape 3: calcul de fij = -U_2'(dij) nij\n",
    "    # Étape 4: somme pour calculer fi\n",
    "    return ..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "872a6edd-2216-4f37-b49b-771bf489c776",
   "metadata": {},
   "source": [
    "Pour finir, intégrez l'appel au bon endroit de la boucle de *Velocity-Verlet*.\n",
    "\n",
    "Afin de tester votre implémentation, vous pourrez calculer à la main la force entre deux particules de rayon $R$ = 1, raideur $k_\\alpha$ = 1, situées à une distance $r$ = 1.5 l'une de l'autre, et comparer à votre implémentation.\n",
    ":::\n",
    "\n",
    "\n",
    "## Ouverture de billard\n",
    "\n",
    "Afin de tester une situation plus complexe, nous allons simuler une ouverture (simplifiée) de billard. On donne les positions et vitesses initiales sous forme de fichiers texte :\n",
    "\n",
    "- <a href=\"data/positions_billard.txt\" download>positions_billard.txt</a>\n",
    "- <a href=\"data/vitesses_billard.txt\" download>vitesses_billard.txt</a>\n",
    "\n",
    "::: {.callout-caution title=\"Consigne\"}\n",
    "Utilisez la fonction [`np.loadtxt`](https://numpy.org/doc/stable/reference/generated/numpy.loadtxt.html) pour lire les fichiers^[Les fichiers doivent être enregistrés dans le même dossier que vos scripts.]. On définit les paramètres du système suivants (attention aux unités, vérifiez les unités des fichiers des positions et vitesses):\n",
    "\n",
    "- Rayons des particules : $R$ = 2.8 cm\n",
    "- Raideur des collisions : $k_\\alpha$ = 1 N/cm\n",
    "- Masses des particules : $m$ = 0.16 kg\n",
    "\n",
    "Calculez le pas de temps critique $\\Delta t_\\mathrm{crit} = \\frac{2}{\\omega}$.\n",
    "\n",
    "Simulez l'évolution du système sans la force de gravité (suffisamment longtemps pour une collision, cf. @fig-collision-force) et visualisez la trajectoire avec Ovito et la fonction `write_xyz` utilisée au TP précédent.\n",
    "\n",
    "Vérifiez que la quantité de mouvement avant la collision est conservée après la collision. Vous pourrez implémenter une fonction `momentum` de la masse et de la vitesse qui fait le calcul de la quantité de mouvement.\n",
    "\n",
    "Calculez l'énergie cinétique du système à la fin de chaque pas de temps, et affichez l'évolution de l'énergie cinétique au cours du temps. Cette dernière est-elle constante ? Essayez différentes valeurs du pas de temps et commentez la différence.\n",
    ":::\n",
    "\n",
    "::: {.content-visible when-format=\"html\"}\n",
    "![Visualisation de la collision et des forces de répulsion entre boules de billard](figs/collision_force.apng){#fig-collision-force}\n",
    ":::\n",
    "\n",
    "::: {.content-visible when-format=\"pdf\"}\n",
    "![Visualisation de la collision et des forces de répulsion entre boules de billard](figs/collision_force.png){#fig-collision-force}\n",
    ":::\n",
    "\n",
    "## Contact élastique avec un mur\n",
    "\n",
    "Le contact élastique d'une particule avec un objet immobile est relativement simple à traiter si l'on admet d'approximer l'instant et l'endroit de la collision. Puisque le choc est élastique, les énergies cinétiques avant et après choc doivent être égales. Pour une particule avec une vitesse incidente (avant choc) $\\vec v_i$, seule la composante du vecteur vitesse qui est normale à la surface rigide est affectée. On note $\\vec v_r$ la vitesse réfléchie. Dans le cas d'une surface plane, on a le schéma suivant :\n",
    "\n",
    "![Rebond élastique sur une surface immobile](figs/contact_mur.svg)\n",
    "\n",
    "Puisque la composante de vitesse parallèle au mur $v_{i,x} = |\\vec v_i|\\cos(\\theta_i) = v_{r,x}$ n'est pas affectée, on a $\\theta_i = \\theta_r$, comme pour la réflexion d'une onde plane. La composante verticale de la vitesse réfléchie est donnée par $v_{r,y} = -v_{i,y}$.\n",
    "\n",
    "::: {.callout-caution title=\"Consigne\"}\n",
    "\n",
    "En reprenant le code de l'ouverture de billard, implémentez les collisions avec un sol plat situé en contrebas de la position initiale des particules, et ajoutez la force de gravité à la force due aux chocs élastiques. Pour chaque particule, il faudra déterminer si la particule est au-dessus ou en dessous du plan du mur, puis modifier le vecteur vitesse si besoin.\n",
    "\n",
    "*Note : il faut déterminer quel est l'endroit, parmi les étapes de l'algorithme Velocity-Verlet, où il est judicieux de modifier la vitesse. Essayez les différentes possibilités et sélectionnez le résultat le plus physique*.\n",
    "\n",
    ":::"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2bc409f5-117b-4f3f-be1f-82d525387310",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
