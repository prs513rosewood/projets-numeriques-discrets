---
title: "Fissuration"
---

![Fissure en milieu désordonné, [@milaneseAvalanchesDrySaturated2016]](figs/crack_milanese.jpeg)

Dans ce projet, on va s'intéresser à la propagation d'une fissure dans un solide. Les fissures sont des défauts très importants des structures, puisqu'elles réduisent souvent la capacité portante de manière significative.

Une question importante d'ingénierie est de quantifier la taille critique pour une fissure, c'est-à-dire la taille au-delà de laquelle une fissure se propage de façon instable, entrainant la ruine de la structure. En amont, il faudra cependant répondre à des questions de modélisation :

- Comment modéliser un solide avec une fissure existante ?
- Comment augmenter la taille de la fissure ?
- Comment reconnaitre une rupture instable ?

Une fois ces questions traitées (certaines peuvent avoir plusieurs réponses), on s'intéresse à la problématique du sujet :

- Comment la taille critique de la fissure dépend-elle des propriétés du matériau (et autres choix de modélisation) ?
- Le résultat obtenu est-il en accord avec la théorie linéaire élastique de la rupture ?
- Quelle est l'énergie de rupture ? Quel est l'effet de la température ?