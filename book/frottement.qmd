---
title: "Frottement"
---

![Particules reliées par ressorts interagissant avec un potentiel périodique. C'est l'extension Frenkel--Kontorova du modèle de frottement de Prandtl.](figs/prandtl.svg)

Le frottement est un phénomène complexe qui prend sa source dans les interactions entre atomes, molécules et aspérités de deux surfaces. Prandtl est l'un des premiers à avoir idéalisé ces interactions et proposé un modèle qui contient les ingrédients de base du frottement solide: une force statique [@popovPrandtlTomlinsonModelHistory2012] de frottement^[une force qu'il faut surpasser pour causer un glissement macroscopique], une force cinétique [@muserVelocityDependenceKinetic2011] de frottement^[une force à vitesse constante qui résiste le mouvement] et une dissipation d'énergie.

Le modèle considère deux particules: l'une est libre de son mouvement, et est attachée à la seconde par un ressort de raideur $k$. Elle subit une force extérieure dérivant d'un potentiel périodique $F(x) = F_0\sin(2\pi x / a)$ et une force d'amortissement visqueux proportionnelle (avec un coefficient $\eta$) à sa vitesse. La seconde particule se déplace à vitesse constante $v_0$. La position $x$ de la particule libre obéit donc à l'équation suivante:

$$m \frac{\mathrm d^2x}{\mathrm dt^2} = k(v_0 t - x) - \eta\frac{\mathrm dx}{\mathrm dt} - F_0\sin(2\pi x / a). $$

On se pose les questions suivantes:

- Quels sont les nombres sans dimensions qui gouvernent le modèle ?
- Quelle est la force statique de frottement ? Comment dépend-elle des paramètres du modèle ?
- Quelle est la force cinétique de frottement ? Comment dépend-elle des paramètres du modèle ?
- Comment ajouter l'effet de la température dans le modèle ? Comment change-t-elle les résultats précédents ?
- Quelle quantité d'énergie est dissipée pour un mouvement cyclique de la particule ?
- Que se passe-t-il lorsque plusieurs particules interagissent comment dans le modèle de Frenkel--Kontorova ?