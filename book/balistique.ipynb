{
 "cells": [
  {
   "cell_type": "raw",
   "id": "8bca2a64-9a7b-4935-b399-ab3326eef72b",
   "metadata": {
    "editable": true,
    "raw_mimetype": "",
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "---\n",
    "format:\n",
    "    html:\n",
    "        code-fold: false\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "44c68475-30f1-4c53-b307-c7268b3de7e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "#| echo: false\n",
    "\n",
    "%config InlineBackend.figure_formats = ['svg']\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "86271a02-2255-4a12-8a5e-46583d5bc7c4",
   "metadata": {},
   "source": [
    "# 1/ Balistique de $N$ corps\n",
    "\n",
    "![Particules uniformément accélérées](figs/fireworks)\n",
    "\n",
    "Dans ce TP, on mettra en place un code pour prédire la trajectoire dans un champ gravitationnel de N particules ayant des vitesses initiales aléatoires.\n",
    "\n",
    "#### Objectifs\n",
    "- Représenter numériquement les positions et vitesses d'un ensemble de particules\n",
    "- Intégrer les équations du mouvement avec l'algorithme de Verlet\n",
    "- Vérifier l'implémentation avec une solution connue\n",
    "- Visualiser les trajectoires des particules\n",
    "\n",
    "## Mise en place\n",
    "\n",
    "On considère un système à deux dimensions de $N$ particules dont on note les positions $\\{\\vec{r}_i\\}_{i=1}^N$ et les vitesses $\\{\\vec{v}_i\\}_{i=1}^N$.\n",
    "\n",
    "On utilise des *arrays* NumPy pour représenter ces quantités"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad4cd2b2-f971-4fb2-ad94-dda578f21195",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import numpy.typing as npt\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "N = 100   # Nombre de particules\n",
    "D = 2     # Dimension du problème\n",
    "\n",
    "positions = np.zeros([D, N])           # Crée un tableau 2D avec des zéros\n",
    "velocities = np.zeros_like(positions)  # Crée un tableau de la même taille que positions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae564fe6-9ee9-4ad9-b0e8-2b4dd862bfdb",
   "metadata": {},
   "source": [
    "Ainsi, la première dimension de l'*array* `positions` permet de choisir la composante ($x$ ou $y$) de la position de toutes les particules (`positions[0]` donne toutes les positions selon $x$), alors que la deuxième dimension permet de sélectionner le vecteur position d'une seule particule (`positions[:, 9]` donne le vecteur position de la 10ᵉ particule), voir @fig-positions.\n",
    "\n",
    "![Disposition des coordonnées de particules dans le tableau `positions`](figs/positions_layout.svg){#fig-positions}\n",
    "\n",
    "On devra aussi créer un *array* pour les masses des particules et les forces que subissent chaque particule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a059ba85-2cb7-48d0-8279-a7822e2390c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "masses = np.ones([N])  # Crée un tableau 1D avec des 1\n",
    "forces = np.zeros_like(positions)\n",
    "\n",
    "# Pour vérifier les tailles des tableaux\n",
    "print(f\"Taille de `positions`: {positions.shape}, taille de `masses`: {masses.shape}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "703f6260-54c8-4ca8-ad8e-deb43576fa2b",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "Il est important de noter que le tableau `masses` est unidimensionnel, puisque la masse de chaque particule n'est pas un vecteur mais un scalaire.\n",
    "\n",
    "::: {.callout-note title=\"Rappel\"}\n",
    "Avec Numpy, on évite autant que l'on peut de faire des calculs en utilisant des boucles Python. Par exemple, on évitera :\n",
    "\n",
    "```python\n",
    "for i in range(N):\n",
    "    for d in range(D):\n",
    "        positions[d, i] += velocities[d, i] * dt\n",
    "```\n",
    "\n",
    "À la place, on écrira simplement :\n",
    "```python\n",
    "positions += velocities * dt\n",
    "```\n",
    "\n",
    "Cependant, certains *arrays* Numpy ont parfois des tailles incompatibles. Par exemple, si on veut appliquer une translation uniforme à toutes les particules :\n",
    "\n",
    "```python\n",
    "u = np.array([1., 1.])\n",
    "positions += u\n",
    "```\n",
    "\n",
    "On obtient l'erreur suivante :\n",
    "\n",
    "```\n",
    "---------------------------------------------------------------------------\n",
    "ValueError                                Traceback (most recent call last)\n",
    "Cell In[15], line 1\n",
    "----> 1 positions += u\n",
    "\n",
    "ValueError: operands could not be broadcast together with shapes (2,100) (2,) (2,100) \n",
    "```\n",
    "\n",
    "Pour corriger cela *sans écrire de boucle*, on doit utiliser une fonctionnalité de Numpy appelée [*broadcasting*](https://numpy.org/doc/stable/user/basics.broadcasting.html) (voir [cette explication en français](https://perso.univ-lyon1.fr/laurent.martin-witkowski/teaching/MGC2005L/Broadcasting.html)). Ici, on a le souci que le vecteur de translation n'a qu'un axe, et que `positions` en a deux. On peut corriger cela avec `np.newaxis` pour ajouter un axe au vecteur `u`:\n",
    "\n",
    "```python\n",
    "positions += u[:, np.newaxis]\n",
    "```\n",
    "Numpy comprend alors automatiquement que `u` doit être rajouté à toutes les colonnes de `positions`.\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f69179f-f0d9-444d-b7ac-677480c926ec",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## Velocity-Verlet, implémentation\n",
    "\n",
    "On donne les prototypes des fonctions principales de l'algorithme *Velocity-Verlet* ci-dessous.\n",
    "\n",
    ":::{.callout-tip}\n",
    "Si vous n'êtes pas familier avec la syntaxe d'annotation des types en Python (utilisée ci-dessous), voyez @sec-type-hint. *(TLDR ; cela sert à préciser le type d'un argument pour une fonction, mais cela n'affecte pas l'exécution du code.)*\n",
    ":::"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "01d8da0f-7426-452a-98fd-b57a3f65d960",
   "metadata": {},
   "outputs": [],
   "source": [
    "def verlet_predictor(positions: npt.NDArray[float],\n",
    "                     velocities: npt.NDArray[float],\n",
    "                     masses: npt.NDArray[float],\n",
    "                     forces: npt.NDArray[float],\n",
    "                     dt: float):\n",
    "    \"\"\"Étape de prédiction de Velocity-Verlet\"\"\"\n",
    "\n",
    "def verlet_corrector(positions: npt.NDArray[float],\n",
    "                     velocities: npt.NDArray[float],\n",
    "                     masses: npt.NDArray[float],\n",
    "                     forces: npt.NDArray[float],\n",
    "                     dt: float):\n",
    "    \"\"\"Étape de correction de Velocity-Verlet\"\"\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d23bef3e-ede5-4a4d-8069-17fdf1586c11",
   "metadata": {},
   "source": [
    ":::{.callout-caution title=\"Consigne\"}\n",
    "Implémentez les étapes de prédiction et corrections de l'algorithme *Velocity-Verlet*. Pour rappel, l'algorithme de *Velocity-Verlet* s'écrit :\n",
    "\n",
    "1. $\\vec v_{i+1/2} = \\vec v_i + \\frac{\\Delta T}{2} \\left( \\frac{\\vec f_i}{m} \\right)$\n",
    "2. $\\vec r_{i+1} = \\vec r_i + \\Delta T \\vec v_{i+1/2}$\n",
    "3. Calcul des forces (ici les forces sont constantes)\n",
    "4. $\\vec v_{i+1} = \\vec v_{i+1/2} + \\frac{\\Delta T}{2}\\left(\\frac{\\vec f_{i+1}}{m} \\right)$\n",
    "\n",
    "Les étapes 1 et 2 sont les étapes de **prédiction**, l'étape 4 est l'étape de **correction**. La @fig-schema-verlet ci-dessous résume les étapes de calcul.\n",
    "\n",
    "\n",
    "En règle générale, on évite de stocker en mémoire les valeurs de la vitesse et de la position à chaque pas de temps. On peut donc écrire l'algorithme avec des opérations qui modifient les vecteurs:\n",
    "\n",
    "1. $\\vec v \\gets \\vec v + \\frac{\\Delta T}{2} \\left( \\frac{\\vec f}{m} \\right)$\n",
    "2. $\\vec r \\gets \\vec r + \\Delta T \\vec v$\n",
    "3. $\\vec f \\gets \\vec f(\\vec r)$\n",
    "4. $\\vec v  \\gets \\vec v + \\frac{\\Delta T}{2}\\left(\\frac{\\vec f}{m} \\right)$\n",
    "\n",
    "La flèche $\\gets$ symbolise ici le fait que l'on assigne la valeur d'une variable. On pourra utiliser l'opérateur Python `+=` pour faire cette opération et la somme.\n",
    ":::\n",
    "\n",
    "![Schéma de verlet](figs/verlet_steps.svg){#fig-schema-verlet}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4cf12ee-17e6-47f2-b561-41187dccd154",
   "metadata": {},
   "source": [
    "## Test\n",
    "\n",
    "Pour une particule dans un champ gravitationnel $\\vec g = -g \\vec{e}_y$, ayant un vecteur vitesse initial $\\vec v = v_x\\vec e_x + v_y \\vec e_y$, calculez en fonction du temps $t$ la trajectoire analytique."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "953b4cc4-9199-4115-8623-0952c3b1c2a3",
   "metadata": {},
   "outputs": [],
   "source": [
    "def analytical_position(initial_velocity: npt.NDArray[float],\n",
    "                        gravity: npt.NDArray[float],\n",
    "                        t: float):\n",
    "    \"\"\"Calcule la position d'une particule dans un champ gravitationel\"\"\"\n",
    "    final_position = ...\n",
    "    return final_position"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "425e4088-805e-467f-aa03-1f3871195e87",
   "metadata": {},
   "source": [
    "Initialisez les vitesses des particules à des valeurs aléatoires :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cf95ee44-9e36-4a95-af23-d8c2ff0bc0fd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Les lignes suivantes servent uniquement à générer des vitesses initiales aléatoires\n",
    "# Il n'est pas utile de comprendre le détail de ces fonctions\n",
    "rng = np.random.default_rng()  # crée un générateur de nombres aleatoires\n",
    "initial_velocity = rng.uniform(-1, 1, velocities.shape)  # rempli initial_velocity de nombres distribues entre -1 et 1\n",
    "\n",
    "velocities[:] = initial_velocity"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bddffc36-c60d-4887-a3c1-8080fc253288",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    ":::{.callout-caution title=\"Consigne\"}\n",
    "À l'aide de *Velocity-Verlet* et du pas de temps $\\Delta t$ de votre choix, intégrez le mouvement des $N$ particules jusqu'à $t = 1$ s. Pour chaque itération, comparez les positions numériques avec la théorie analytique.\n",
    "\n",
    "```python\n",
    "# Étape 1 - donner la bonne valeur (en fonction de g) aux forces (la gravité s'applique selon y uniquement)\n",
    "\n",
    "# Étape 2 - la boucle d'intégration\n",
    "for i in range(...):\n",
    "    ... # on exécute Velocity-Verlet (prédiction et correction), sans le calcul des forces (elles sont constantes!)\n",
    "\n",
    "positions_analytical = ... # appelez la bonne fonction\n",
    "\n",
    "# Pour tester la position des particules, on peut se servir de np.testing.assert_allclose\n",
    "# Cette fonction donne une erreur si les valeurs ne sont pas identiques\n",
    "tolerance = 1e-14\n",
    "np.testing.assert_allclose(positions, positions_analytical, atol=tolerance)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2a90f98-db4a-4023-be67-f1f57c8f4fd8",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "Enfin, affichez à l'aide de [`plt.plot()`](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html) les trajectoires **d'une seule particule** en 2D.\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "31730695-d198-4e7b-a158-605c6e50f40b",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "## Input-output\n",
    "\n",
    "Matplotlib permet facilement de visualiser des trajectoires en 2D pour un faible nombre de particules, mais est assez limité pour un grand nombre de particules (à moins d'animer la trajectoire au lieu de dessiner sa trace), et surtout ses capacités de visualisation en 3D sont très limitées.\n",
    "\n",
    "Des logiciels spécialisés, tels que [Paraview](https://paraview.org) permettent de visualiser des jeux de données plus gros et plus complexe que matplotlib. Dans ce cours, on utilisera un logiciel spécialisé dans le post-traitement de simulations discrètes appelé [Ovito](https://ovito.org), qui a été conçu en particulier pour la dynamique moléculaire.\n",
    "\n",
    "::: {.callout-tip}\n",
    "Ovito est disponible [ici](https://www.ovito.org/download/master/ovito-basic-3.10.3-win64.zip). Sur les ordinateurs des salles de TP il n'est pas possible de lancer Ovito depuis le navigateur de fichiers. Il faut d'abord \"Extraire\" l'archive ZIP téléchargée, puis ouvrir \"Powershell\" et executer la commande suivante:\n",
    "\n",
    "```powershell\n",
    "~\\Downloads\\ovito-basic-3.10.3-win64\\ovito.exe\n",
    "```\n",
    "\n",
    "Utilisez le clic droit pour coller la commande ci-dessus dans PowerShell.\n",
    ":::\n",
    "\n",
    "Pour pouvoir traiter les données de notre simulation, il faut les écrire dans des fichiers que Ovito pourra lire. Un format commun pour les simulations en dynamique moléculaire est le format [XYZ](https://en.wikipedia.org/wiki/XYZ_file_format)&nbsp;: c'est un fichier texte contenant le nombre de particules sur la première ligne, une ligne de commentaire (ignorée par Ovito) et un tableau de 4 colonnes : la première est le symbole chimique de l'élément, les trois autres les coordonnées `x y z`.\n",
    "\n",
    "La fonction ci-dessous permet d'écrire un fichier XYZ, et suppose que toutes les particules sont des atomes d'hydrogène."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6f56fc96-f7b0-43ec-9e9b-d83d6238d319",
   "metadata": {},
   "outputs": [],
   "source": [
    "def write_xyz(fname: str, positions: npt.NDArray[float], symbol: str = None):\n",
    "    \"\"\"Writes an XYZ file from atom positions (2d or 3d)\"\"\"\n",
    "    natoms = positions.shape[1]\n",
    "\n",
    "    dtype = [('symbols', np.unicode_, 2),\n",
    "             ('positions', float, 3)]\n",
    "    dtype = np.dtype(dtype)\n",
    "    \n",
    "    extended_pos = np.zeros(natoms, dtype=dtype)\n",
    "    extended_pos['positions'][:, :positions.shape[0]] = positions.T\n",
    "    extended_pos['symbols'][:] = 'H' if symbol is None else symbol\n",
    "    \n",
    "    with open(fname, 'w') as fh:\n",
    "        fh.write(str(natoms) + '\\n\\n')\n",
    "        for row in extended_pos:\n",
    "            s, x = row['symbols'], row['positions']\n",
    "            fh.write(f\"{s} {x[0]:.18e} {x[1]:.18e} {x[2]:.18e}\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8c6e177-4d0c-446b-ac2b-663dab315f36",
   "metadata": {},
   "source": [
    ":::{.callout-caution title=\"Consigne\"}\n",
    "\n",
    "Servez-vous de la fonction ci-dessus pour écrire une séquence de fichiers `traj_0000.xyz`, `traj_0001.xzy`, etc. Pour rappel, on peut utiliser les [*f-strings*](https://docs.python.org/3/tutorial/inputoutput.html#formatted-string-literals) Python pour formater le numéro de l'itération, par exemple :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d3f4ffd-869c-4cec-9dc1-91c526fd1fac",
   "metadata": {},
   "outputs": [],
   "source": [
    "i = 21\n",
    "print(f\"traj_{i:04d}.xyz\") # affiche le numéro avec 4 chiffres"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4e67618-f5e9-4303-9494-83ec041ee0c4",
   "metadata": {},
   "source": [
    "Ensuite, utilisez Ovito pour ouvrir le premier fichier (`traj_0000.xyz`), et visualisez le mouvement des particules.\n",
    ":::\n",
    "\n",
    "![Interface Ovito](figs/ovito_fireworks.png)\n",
    "\n",
    "\n",
    ":::{.callout-tip}\n",
    "Si vous avez du mal à voir le mouvement des particules sur Ovito, réduisez l'accélération de la gravité.\n",
    ":::"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "923aedae-ecee-4889-b286-d86e6dc11bd5",
   "metadata": {},
   "source": [
    "### Résumé des consignes\n",
    "\n",
    "1. Implémenter l'algorithme *Velocity-Verlet* pour $N$ particules (en 2D ou en 3D)\n",
    "2. Calculer l'expression analytique d'une particule avec vitesse initiale dans un champ gravitationnel uniforme\n",
    "3. Comparer l'implémentation à la solution analytique\n",
    "4. Afficher les trajectoires des particules\n",
    "    1. En 2D : avec matplotlib\n",
    "    2. En 2D et 3D : écrire des fichiers XYZ et visualiser avec Ovito\n",
    "5. (Bonus) [générer une video](https://www.ovito.org/docs/current/usage/rendering.html) avec Ovito"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "deb21f04-9f60-4663-a793-bbdcd59f07cf",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
