project:
  type: book

lang: fr
toc: true
number-sections: true
bibliography: biblio.bib

book:
  title: "Modélisation discrète des solides et fluides"
  author: "Lucas Frérot"
  language: fr
  chapters:
    - index.qmd
    - part: "Théorie"
      chapters:
        - equations_de_mouvement.ipynb
        - interactions.ipynb
        - lj.ipynb
        - sph.ipynb
    - part: "Pratique"
      chapters:
        - prep.ipynb
        - balistique.ipynb
        - collisions_entre_particules.ipynb
        - lj_tp.ipynb
        - thermostat.ipynb
        - truss.ipynb
    - part: "Projets"
      chapters:
        - plaque.qmd
        - fissure.qmd
        # - granulaire.qmd
        - fontis.qmd
        - surface.qmd
        - mosh_pit.qmd
        - fluides.ipynb
        - frottement.qmd
  appendices:
    - report.qmd
  references: biblio.bib

  downloads: pdf
  page-footer: "Copyright © 2023-2024 Lucas Frérot, ce travail est placé sous licence [CC
  BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1). Aucune donnée personnelle n'est collectée. Fait avec [Quarto](https://quarto.org/) et [JupyterLab](https://jupyter.org/)."
  repo-url: https://gitlab.com/prs513rosewood/projets-numeriques-discrets
  repo-actions: [issue]
  image: figs/balls_water.webp
  favicon: favicon.svg


format:
  html:
    theme: cosmo
    code-fold: true
    code-tools: true
    code-annotations: hover
    echo: true
    fig-align: center
    fig-cap-location: margin
    link-external-icon: true
    link-external-newwindow: true
    citations-hover: true
    footnotes-hover: true
    highlight-style: github
    number-depth: 2
    default-image-extension: webp
    embed-resources: false

  pdf:
    classoption: [DIV=13]
    mainfont: "TeX Gyre Pagella"
    sansfont: "TeX Gyre Heros"
    sansfontoptions:
      - Color=00374F
    default-image-extension: pdf
    fig-format: pdf
    pdf-engine: xelatex
    pdf-engine-opt: '-shell-escape'
    keep-tex: True
    include-in-header:
      text: |
        \usepackage{grffile}

execute:
  freeze: auto
